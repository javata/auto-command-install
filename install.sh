#!/bin/sh

check_and_create_ssh_key () {
        if [ -f ~/.ssh/id_rsa.pub ]; then
                echo "File exist."
        else
                echo "File doest not exist."
                echo -e "\n" | ssh-keygen -t rsa -N ""
        fi
        echo $'\nPlease register ssh key to git server\n'
        echo "======================================"
        cat ~/.ssh/id_rsa.pub
        echo "======================================"
}

git_clone_auto_command () {

        if ! yum list installed git >/dev/null 2>&1; then
                sudo yum install git -y
        fi
        
        if ! type "pip" > /dev/null; then
                curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
                sudo python get-pip.py
                rm -f get-pip.py
        fi

        if ! python -c "import six.moves" &> /dev/null; then
                sudo pip install six
        fi
        
        if ! python -c "import openpyxl" &> /dev/null; then
                sudo pip install openpyxl
                sudo pip install --upgrade lxml
        fi
        
        if ! python -c "import fabric" &> /dev/null; then
                sudo pip install fabric==1.14.1
                sudo pip install cryptography==2.4.2
        fi
        
        if [ ! -f ~/.ssh/config ] || ! grep -qF "dev-builder.trumpia.com" ~/.ssh/config > /dev/null
        then
                echo -e "Host dev-builder.trumpia.com\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config
        fi

        chmod 600 ~/.ssh/config
        git clone git@gitlab.trumpia.com:Middleware/auto-command.git
        sudo yum install -y epel-release
        sudo yum install -y https://repo.ius.io/ius-release-el7.rpm
        sudo yum install -y python35u python35u-devel python35u-libs python35u-pip python35u-setuptools
        sudo yum update -y
        sudo pip install --upgrade pip
        sudo pip3.5 install virtualenv virtualenvwrapper
        echo "export WORKON_HOME=~/.virtualenvs" >> .bashrc
        echo "export VIRTUALENVWRAPPER_PYTHON=/bin/python3.5" >>  .bashrc
        echo "source /usr/bin/virtualenvwrapper.sh" >> .bashrc
        source ~/.bashrc
        mkvirtualenv py35
        source $WORKON_HOME/py35/bin/activate
        pip install django
        pip install django_tables2
        pip install cx_Oracle
        pip install fabric3
        pip install pymongo
        pip install pymysql
        sudo tee /etc/yum.repos.d/MariaDB.repo<<EOF 
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.4/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
EOF
        sudo yum install -y MariaDB-devel
        sudo yum install -y python-devel mysql-devel
        sudo yum install -y gcc
        sudo yum install -y MariaDB-client
        sudo yum install -y MariaDB-shared
        pip install mysqlclient
        sudo yum install -y http://192.168.3.206/oracle/oracle-instantclient19.5-basic-19.5.0.0.0-1.x86_64.rpm
}

print_menu () {
        echo "1) check and create ssh key"
        echo "2) git clone auto-command"
        echo "3) Quit"
}

PS3='Please enter your choice: '
options=("check and create ssh key" "git clone auto-command" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "check and create ssh key")
            echo "check and create ssh key"
            check_and_create_ssh_key
            print_menu
            ;;
        "git clone auto-command")
            echo "git clone auto-command"
            git_clone_auto_command
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done